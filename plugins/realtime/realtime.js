(function ($) {

  Drupal.wysiwyg.plugins['realtime'] = {


    /**
     * When the button is clicked.
     */
    invoke: function (data, settings, instanceId) {
      var instance = this.getInstance(instanceId);
      // I don't think this is possible in a generic Drupal sense.
      instance.realtime_previous_content = '';
      var self = this;

      setInterval(function () {
        var content = instance.getContent();
        if (content != instance.realtime_previous_content) {
          self.contentChanged(data, settings, instanceId);
        }
        instance.realtime_previous_content = content;
      }, 1);
    },

    /**
     * Content was changed.
     */
    contentChanged: function (data, settings, instanceId) {
      var instance = this.getInstance(instanceId);
      var content = instance.getContent();

      if (typeof instance.realtime_model_string != 'undefined') {
        instance.realtime_model_string.setText(content);
      }
    },

    /**
     * When the WYSIWYG needs attaching.
     */
    attach: function (content, settings, instanceId) {
      var instance = this.getInstance(instanceId);

      console.log(settings);

      if (typeof instance['realtime_started'] == 'undefined' || instance.realtime_started == false) {
        instance.realtime_started = true;
      }
      else {
        return content;
      }

      var auth_button_id = this.create_auth_button(instanceId);

      var realtimeOptions = {
        clientId: '',
        authButtonElementId: auth_button_id,
        initializeModel: this.initialize_model.bind(instance),
        autoCreate: true,
        defaultTitle: "New Realtime Quickstart File",
        newFileMimeType: null,
        onFileLoaded: this.on_file_load.bind(instance),
        registerTypes: null,
        afterAuth: null,
        drupalFileName: settings.page_path + '/' + instanceId
      };

      var realtimeLoader = new rtclient.RealtimeLoader(realtimeOptions);
      realtimeLoader.start();

      return content;
    },

    on_file_load: function (doc) {
      var string = doc.getModel().getRoot().get('text');
      var instance = this;

      instance.realtime_model_string = string;

      string.addEventListener(gapi.drive.realtime.EventType.TEXT_INSERTED, function () {
        console.log('Detected change with ' + string.getText());
        instance.setContent(string.getText());
      });
      string.addEventListener(gapi.drive.realtime.EventType.TEXT_DELETED, function () {
        instance.setContent(string.getText());
      });
    },

    initialize_model: function (model) {
      var string = model.createString('Hello Realtime World!');
      model.getRoot().set('text', string);
    },

    create_auth_button: function (instanceId) {
      var auth_id = 'button-' + instanceId;
      var $button = $('<button id="' + auth_id + '" disabled>You must authorize</button>');
      $('body').append($button);
      return auth_id;
    },

    detach: function (content, settings, instanceId) {
      return content;
    },
    isNode: function (node) {
      return false;
    },

    getInstance: function(instanceId) {
      return Drupal.wysiwyg.instances[instanceId];
    }
  };

})(jQuery);
