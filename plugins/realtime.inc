<?php

/**
 * Implements hook_wysiwyg_plugin().
 */
function realtime_collaboration_realtime_plugin() {
  $plugins['realtime'] = array(
    'title' => t('Realtime Collab'),
    'vendor url' => 'http://drupal.org/project/wysiwyg',
    'icon file' => 'break.gif',
    'icon title' => t('Separate the teaser and body of this content'),
    'settings' => array(
      'page_path' => $_GET['q'],
    ),
  );
  return $plugins;
}
